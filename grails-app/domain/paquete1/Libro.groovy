package paquete1

class Libro {

    String titulo
    String editorial
    static belongsTo = [autor:Autor]
    static constraints = {
        editorial nullable:true
    }
}
